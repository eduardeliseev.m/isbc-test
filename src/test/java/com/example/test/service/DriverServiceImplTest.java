package com.example.test.service;

import com.example.test.model.Driver;
import com.example.test.repository.DriverCrudRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doReturn;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
class DriverServiceImplTest {
    /**
     * Autowire сервис для тестов
     */
    @Autowired
    private DriverServiceImpl driverService;

    /**
     * Mock имплементация Driver
     */
    @MockBean
    private DriverCrudRepository driverRepository;

    @Test
    void findAll() {
        Driver driver1 = new Driver("test", "testov", "53i54390", new Date());
        Driver driver2 = new Driver("test2", "testov", "53i54390", new Date());
        doReturn(Arrays.asList(driver1, driver2)).when(driverRepository).findAll();

        List<Driver> drivers = driverService.findAll();

        Assertions.assertEquals(2, drivers.size(), "findAll should return 2 drivers");
    }

    @Test
    void findDriverById() {
        Driver driver = new Driver("test", "testov", "53i54390", new Date());
        doReturn(Optional.of(driver)).when(driverRepository).findById(1L);

        Optional<Driver> savedDriver = driverRepository.findById(1L);

        Assertions.assertTrue(savedDriver.isPresent(), "Driver was not found");
        Assertions.assertSame(savedDriver.get(), driver, "The driver returned was not the same as the mock");
    }

    @Test
    void findDriverByIdNotFound() {
        doReturn(Optional.empty()).when(driverRepository).findById(1L);

        Optional<Driver> returnedDriver = driverRepository.findById(1L);

        Assertions.assertFalse(returnedDriver.isPresent(), "Driver should not be found");
    }

    @Test
    void saveDriver() {
        Driver driver = new Driver("test", "testov", "53i54390", new Date());
        doReturn(driver).when(driverRepository).save(any());

        Driver returnedDriver = driverService.saveDriver(driver);

        Assertions.assertNotNull(returnedDriver, "Saved driver should not be null");
        Assertions.assertEquals("test", returnedDriver.getFirstName(), "Names does not equals");
    }
}