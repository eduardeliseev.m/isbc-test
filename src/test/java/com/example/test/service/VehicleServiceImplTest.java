package com.example.test.service;

import com.example.test.model.Vehicle;
import com.example.test.repository.VehicleCrudRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
class VehicleServiceImplTest {
    /**
     * Autowire сервис для тестов
     */
    @Autowired
    private VehicleServiceImpl vehicleService;

    /**
     * Mock имплементация Driver
     */
    @MockBean
    private VehicleCrudRepository vehicleRepository;

    @Test
    void findAll() {
        Vehicle vehicle1 = new Vehicle();
        Vehicle vehicle2 = new Vehicle();

        doReturn(Arrays.asList(vehicle1, vehicle2)).when(vehicleRepository).findAll();

        List<Vehicle> vehicles = vehicleService.findAll();

        Assertions.assertEquals(2, vehicles.size(), "findAll should return 2 vehicles");
    }

    @Test
    void findVehicleById() {
        Vehicle vehicle = new Vehicle();
        doReturn(Optional.of(vehicle)).when(vehicleRepository).findById(1L);

        Optional<Vehicle> savedDriver = vehicleRepository.findById(1L);

        Assertions.assertTrue(savedDriver.isPresent(), "Vehicle was not found");
        Assertions.assertSame(savedDriver.get(), vehicle, "The vehicles license returned was not the same as the mock");
    }

    @Test
    void findVehicleByIdNotFound() {
        doReturn(Optional.empty()).when(vehicleRepository).findById(1L);

        Optional<Vehicle> returnedDriver = vehicleRepository.findById(1L);

        Assertions.assertFalse(returnedDriver.isPresent(), "Driver should not be found");
    }

    @Test
    void saveVehicle() {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleType(Vehicle.VehicleType.AUTO);
        doReturn(vehicle).when(vehicleRepository).save(any());

        Vehicle returnedVehicle = vehicleService.saveVehicle(vehicle);

        Assertions.assertNotNull(returnedVehicle, "Saved vehicle should not be null");
        Assertions.assertEquals("AUTO", returnedVehicle.getVehicleType().toString(), "Types does not equals");
    }
}