package com.example.test.service;

import com.example.test.model.DriversLicense;
import com.example.test.repository.DriversLicenseCrudRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doReturn;
import static org.mockito.ArgumentMatchers.any;


@SpringBootTest
class DriversLicenseServiceImplTest {
    /**
     * Autowire сервис для тестов
     */
    @Autowired
    private DriversLicenseServiceImpl driversLicenseService;

    /**
     * Mock имплементация Driver
     */
    @MockBean
    private DriversLicenseCrudRepository driverRepository;

    @Test
    void findAll() {
        DriversLicense driver1 = new DriversLicense(new Date(), new Date());
        DriversLicense driver2 = new DriversLicense(new Date(), new Date());
        doReturn(Arrays.asList(driver1, driver2)).when(driverRepository).findAll();

        List<DriversLicense> drivers = driversLicenseService.findAll();

        Assertions.assertEquals(2, drivers.size(), "findAll should return 2 drivers licenses");
    }

    @Test
    void findDriversLicenseById() {
        DriversLicense driversLicense = new DriversLicense(new Date(), new Date());
        doReturn(Optional.of(driversLicense)).when(driverRepository).findById(1L);

        Optional<DriversLicense> savedDriver = driverRepository.findById(1L);

        Assertions.assertTrue(savedDriver.isPresent(), "Driver was not found");
        Assertions.assertSame(savedDriver.get(), driversLicense, "The drivers license returned was not the same as the mock");
    }

    @Test
    void findDriversLicenseByIdNotFound() {
        doReturn(Optional.empty()).when(driverRepository).findById(1L);

        Optional<DriversLicense> returnedDriver = driverRepository.findById(1L);

        Assertions.assertFalse(returnedDriver.isPresent(), "Driver should not be found");
    }

    @Test
    void saveDriversLicense() {
        DriversLicense driversLicense = new DriversLicense(new Date(), new Date());
        driversLicense.setLicenseType(DriversLicense.LicenseType.B);
        doReturn(driversLicense).when(driverRepository).save(any());

        DriversLicense returnedDriversLicense = driversLicenseService.saveDriversLicense(driversLicense);

        Assertions.assertNotNull(returnedDriversLicense, "Saved driversLicense should not be null");
        Assertions.assertEquals("B", returnedDriversLicense.getLicenseType().toString(), "Types does not equals");
    }
}