package com.example.test.controller;

import com.example.test.mapper.VehicleMapper;
import com.example.test.model.Vehicle;
import com.example.test.service.VehicleServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class VehicleControllerTest {

    @MockBean
    private VehicleServiceImpl service;

    @Autowired
    private VehicleMapper vehicleMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("GET /vehicles success")
    void all() throws Exception {
        Vehicle vehicle1 = new Vehicle();
        Vehicle vehicle2 = new Vehicle();

        vehicle1.setDateOfCreation(new Date());
        vehicle2.setDateOfCreation(new Date());

        doReturn(Lists.newArrayList(vehicle1, vehicle2)).when(service).findAll();

        vehicle1.setVehicleType(Vehicle.VehicleType.AUTO);

        service.saveVehicle(vehicle1);
        service.saveVehicle(vehicle2);

        mockMvc.perform(get("/vehicles"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].vehicleType", is(Vehicle.VehicleType.AUTO.toString())));

    }

    @Test
    void createVehicle() throws Exception {
        Vehicle vehicle1 = new Vehicle();
        doReturn(vehicle1).when(service).saveVehicle(any());
        vehicle1.setVehicleType(Vehicle.VehicleType.AUTO);

        String postString = "{" +
                "\"dateOfCreation\":\"2012-12-12\""
                + "}";

        mockMvc.perform(post("/vehicles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(postString))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.vehicleType", is(Vehicle.VehicleType.AUTO.toString())));
    }

    @Test
    void one() throws Exception {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleId(1L);
        vehicle.setDateOfCreation(new Date());

        when(service.findVehicleById(1L)).thenReturn(vehicle);

        mockMvc.perform(get("/vehicles/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.vehicleId", is(1)));

    }

    @Test
    void updateVehicle() throws Exception {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleId(1L);

        when(service.findVehicleById(1L)).thenReturn(vehicle);

        String updateString = "{" +
                "\"dateOfCreation\":\"2012-12-12\""
                + "}";

        mockMvc.perform(put("/vehicles/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateString))
                .andExpect(status().isOk());
    }

}