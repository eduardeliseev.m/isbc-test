package com.example.test.controller;

import com.example.test.model.Driver;
import com.example.test.model.DriversLicense;
import com.example.test.model.Vehicle;
import com.example.test.service.DriverServiceImpl;
import com.example.test.service.DriversLicenseServiceImpl;
import com.example.test.service.VehicleServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class DriverControllerTest {

    @MockBean
    private DriverServiceImpl service;

    @MockBean
    private VehicleServiceImpl vehicleService;

    @MockBean
    private DriversLicenseServiceImpl driversLicenseService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("GET /drivers success")
    void all() throws Exception {
        Driver driver1 = new Driver("test", "testov", "53i54390", new Date());
        Driver driver2 = new Driver("test2", "testov", "53i54390", new Date());
        doReturn(Lists.newArrayList(driver1, driver2)).when(service).findAll();

        mockMvc.perform(get("/drivers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].firstName", is("test")));

    }

    @Test
    void createDriver() throws Exception {
        Driver driver1 = new Driver("test", "testov", "53i54390", new Date());
        doReturn(driver1).when(service).saveDriver(any());

        String postString = "{" +
                "\"firstName\":\"test\"," +
                "\"lastName\":\"testov\"," +
                "\"phone\":\"53i54390\"," +
                "\"dateOfBirth\":\"2012-12-12\""
                + "}";

        mockMvc.perform(post("/drivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(postString))

                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName", is("test")));

    }

    @Test
    void one() throws Exception {
        Driver driver = new Driver("test", "testov", "53i54390", new Date());

        when(service.findDriverById(1L)).thenReturn(driver);

        mockMvc.perform(get("/drivers/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName", is("test")));

    }

    @Test
    void updateDriver() throws Exception {
        Driver driver = new Driver("test", "testov", "53i54390", new Date());

        when(service.findDriverById(1L)).thenReturn(driver);

        String updateString = "{" +
                "\"firstName\":\"test2\"," +
                "\"lastName\":\"testov\"," +
                "\"phone\":\"53i54390\"," +
                "\"dateOfBirth\":\"2012-12-12\""
                + "}";

        mockMvc.perform(put("/drivers/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateString))
                .andExpect(status().isOk());
    }

    @Test
    void attachVehicle() throws Exception {
        DriversLicense driversLicense1 = new DriversLicense(new Date(), new Date());
        Driver driver1 = new Driver("test", "testov", "53i54390", new Date());
        Vehicle vehicle1 = new Vehicle();

        driversLicense1.setLicenseType(DriversLicense.LicenseType.B);
        driversLicense1.setDriversLicenseId(1L);
        driver1.setDriversLicense(driversLicense1);
        driver1.setDriverId(1L);
        vehicle1.setVehicleId(1L);
        vehicle1.setVehicleType(Vehicle.VehicleType.AUTO);

        when(service.findDriverById(1L)).thenReturn(driver1);
        when(vehicleService.findVehicleById(1L)).thenReturn(vehicle1);

        doReturn(driver1).when(service).saveDriver(any());
        doReturn(vehicle1).when(vehicleService).saveVehicle(any());

        String postString = "{" +
                "\"driverId\":\"1\"," +
                "\"vehicleId\":\"1\"" +
                "}";

        mockMvc.perform(post("/attach-vehicle/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(postString))

                .andExpect(status().isOk());

    }
}