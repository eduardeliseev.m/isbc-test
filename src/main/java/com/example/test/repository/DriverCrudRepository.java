package com.example.test.repository;

import com.example.test.model.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Репозиторий для работы с таблицей водителей
 */
@Repository
public interface DriverCrudRepository extends JpaRepository<Driver, Long> {}
