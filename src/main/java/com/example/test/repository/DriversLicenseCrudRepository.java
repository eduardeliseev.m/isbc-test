package com.example.test.repository;

import com.example.test.model.DriversLicense;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Репозиторий для работы с таблицей водительских прав
 */
public interface DriversLicenseCrudRepository extends JpaRepository<DriversLicense, Long> {}