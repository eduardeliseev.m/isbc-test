package com.example.test.repository;

import com.example.test.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Репозиторий для работы с таблицей авто
 */
public interface VehicleCrudRepository extends JpaRepository<Vehicle, Long> { }
