package com.example.test.mapper;

import com.example.test.dto.DriversLicenseRequestDto;
import com.example.test.dto.DriversLicenseResponseDto;
import com.example.test.exceptions.DriversLicenseNotFoundException;
import com.example.test.model.DriversLicense;
import com.example.test.service.DriversLicenseService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Service
/**
 * Маппер для коверта водительских прав из модели в дто и обратно
 * @version 1.0
 */
public class DriversLicenseMapper {
    public final ModelMapper modelMapper;

    public final DriversLicenseService driversLicenseService;

    public DriversLicenseMapper(DriversLicenseService driversLicenseService, ModelMapper modelMapper) {
        this.driversLicenseService = driversLicenseService;
        this.modelMapper = modelMapper;
    }

    public DriversLicenseResponseDto convertToDto(DriversLicense driversLicense) {
        DriversLicenseResponseDto driversLicenseDto = modelMapper.map(driversLicense, DriversLicenseResponseDto.class);
        Date date = driversLicense.getDateOfIssue();
        // приводим дату
        LocalDate localDateOfIssue = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDateOfExpiry = driversLicense.getDateOfExpiry().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        driversLicenseDto.setDateOfIssue(localDateOfIssue);
        driversLicenseDto.setDateOfExpiry(localDateOfExpiry);
        return driversLicenseDto;
    }

    public DriversLicense convertToEntity(DriversLicenseRequestDto driversLicenseDto) throws DriversLicenseNotFoundException {
        DriversLicense driversLicense = new DriversLicense();
        return this.convertToEntity(driversLicenseDto, driversLicense);
    }

    public DriversLicense convertToEntity(DriversLicenseRequestDto driversLicenseDto,
                                          DriversLicense driversLicense) throws DriversLicenseNotFoundException {
        modelMapper.map(driversLicenseDto, driversLicense);
        // приводим дату
        Date dateOfIssue  = Date.from(driversLicenseDto.getDateOfIssue().atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date dateOfExpiry = Date.from(driversLicenseDto.getDateOfExpiry().atStartOfDay(ZoneId.systemDefault()).toInstant());

        driversLicense.setDateOfIssue(dateOfIssue);
        driversLicense.setDateOfExpiry(dateOfExpiry);

        return driversLicense;
    }
}
