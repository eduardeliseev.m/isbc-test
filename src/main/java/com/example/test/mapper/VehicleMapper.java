package com.example.test.mapper;

import com.example.test.dto.VehicleRequestDto;
import com.example.test.dto.VehicleResponseDto;
import com.example.test.exceptions.VehicleNotFoundException;
import com.example.test.model.Vehicle;
import com.example.test.service.VehicleService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Маппер для конверта авто из модели в дто и обратно
 * @version 1.0
 */
@Service
public class VehicleMapper {
    public final ModelMapper modelMapper;

    public final VehicleService vehicleService;

    public VehicleMapper(ModelMapper modelMapper, VehicleService vehicleService) {
        this.modelMapper = modelMapper;
        this.vehicleService = vehicleService;
    }

    public VehicleResponseDto convertToDto(Vehicle vehicle) {
        VehicleResponseDto vehicleDto = modelMapper.map(vehicle, VehicleResponseDto.class);

        Date dateOfCreation = vehicle.getDateOfCreation();
        if (dateOfCreation != null) {
            LocalDate localDateOfCreation = vehicle.getDateOfCreation().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            vehicleDto.setDateOfCreation(localDateOfCreation);
        }

        return vehicleDto;
    }

    public Vehicle convertToEntity(VehicleRequestDto vehicleDto) throws VehicleNotFoundException {
        Vehicle vehicle = new Vehicle();
        return this.convertToEntity(vehicleDto, vehicle);
    }

    public Vehicle convertToEntity(VehicleRequestDto vehicleDto, Vehicle vehicle) throws VehicleNotFoundException {
        modelMapper.map(vehicleDto, vehicle);

        LocalDate localDateOfCreation = vehicleDto.getDateOfCreation();

        if (localDateOfCreation != null) {
            Date dateOfCreation  = Date.from(localDateOfCreation.atStartOfDay(ZoneId.systemDefault()).toInstant());
            vehicle.setDateOfCreation(dateOfCreation);
        }

        return vehicle;
    }
}
