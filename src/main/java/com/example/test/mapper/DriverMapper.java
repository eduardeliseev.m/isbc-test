package com.example.test.mapper;

import com.example.test.dto.DriverRequestDto;
import com.example.test.dto.DriverResponseDto;
import com.example.test.dto.DriversLicenseResponseDto;
import com.example.test.exceptions.DriverNotFoundException;
import com.example.test.model.Driver;
import com.example.test.model.DriversLicense;
import com.example.test.service.DriverService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Маппер для конверта водителя из модели в дто и обратно
 * @version 1.0
 */
@Service
public class DriverMapper {
    public final ModelMapper modelMapper;

    public final DriverService driverService;

    public final DriversLicenseMapper driversLicenseMapper;

    public DriverMapper(DriverService driverService,
                        ModelMapper modelMapper,
                        DriversLicenseMapper driversLicenseMapper) {
        this.driverService = driverService;
        this.modelMapper = modelMapper;
        this.driversLicenseMapper = driversLicenseMapper;
    }

    public DriverResponseDto convertToDto(Driver driver) {
        DriverResponseDto driverDto = modelMapper.map(driver, DriverResponseDto.class);
        DriversLicense driversLicense = driver.getDriversLicense();

        // конвертируем вложенную сущность
        if (driversLicense != null) {
            DriversLicenseResponseDto driversLicenseDto = this.driversLicenseMapper.convertToDto(driversLicense);
            driverDto.setDriversLicense(driversLicenseDto);
        }

        // приводим дату
        LocalDate date = driver.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        driverDto.setDateOfBirth(date);

        return driverDto;
    }

    public Driver convertToEntity(DriverRequestDto driverDto) throws DriverNotFoundException {
        Driver driver = new Driver();
        return this.convertToEntity(driverDto, driver);
    }

    public Driver convertToEntity(DriverRequestDto driverDto, Driver driver) throws DriverNotFoundException {
        modelMapper.map(driverDto, driver);
        // приводим дату
        Date date = Date.from(driverDto.getDateOfBirth().atStartOfDay(ZoneId.systemDefault()).toInstant());
        driver.setDateOfBirth(date);
        return driver;
    }
}
