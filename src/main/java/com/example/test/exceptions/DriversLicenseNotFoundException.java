package com.example.test.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DriversLicenseNotFoundException extends RuntimeException {

    public DriversLicenseNotFoundException(Long id) {
        super("Could not find drivers license with id = " + id);
    }
}
