package com.example.test.service;

import com.example.test.exceptions.VehicleNotFoundException;
import com.example.test.model.Vehicle;
import com.example.test.repository.VehicleCrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {
    private final VehicleCrudRepository repository;

    public VehicleServiceImpl(VehicleCrudRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Vehicle> findAll() {
        return repository.findAll();
    }

    @Override
    public Vehicle findVehicleById(Long id) throws VehicleNotFoundException {
        return repository.findById(id)
                .orElseThrow(() -> new VehicleNotFoundException(id));
    }

    @Override
    public Vehicle saveVehicle(Vehicle vehicle) {
        return repository.save(vehicle);
    }

    @Override
    public void deleteVehicleById(Long id) throws VehicleNotFoundException {
        repository.delete(this.findVehicleById(id));
    }

}
