package com.example.test.service;

import com.example.test.exceptions.DriverNotFoundException;
import com.example.test.exceptions.InvalidLicenseCategoryException;
import com.example.test.exceptions.LicenseExpiredException;
import com.example.test.model.Driver;
import com.example.test.model.DriversLicense;
import com.example.test.model.Vehicle;
import com.example.test.repository.DriverCrudRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class DriverServiceImpl implements DriverService {
    private final DriverCrudRepository repository;
    private final VehicleServiceImpl vehicleService;

    public DriverServiceImpl(DriverCrudRepository repository, VehicleServiceImpl vehicleService) {
        this.repository = repository;
        this.vehicleService = vehicleService;
    }

    @Override
    public List<Driver> findAll() {
        return repository.findAll();
    }

    @Override
    public Driver findDriverById(Long id) throws DriverNotFoundException {
        return repository.findById(id)
                .orElseThrow(() -> new DriverNotFoundException(id));
    }

    @Override
    public Driver saveDriver(Driver driver) {
        return repository.save(driver);
    }

    @Override
    public void deleteDriverById(Long id) throws DriverNotFoundException {
        repository.delete(this.findDriverById(id));
    }

    /**
     * Метод привязки авто к водителю
     * @param driverId Id водителя
     * @param vehicleId Id авто
     */
    @Override
    public void attachVehicle(Long driverId, Long vehicleId) {
        Driver driver = repository.findById(driverId)
                .orElseThrow(() -> new DriverNotFoundException(driverId));

        // получаем тип авто
        Vehicle vehicle = vehicleService.findVehicleById(vehicleId);
        String vehicleType = vehicle.getVehicleType().toString();

        // получаем текущие активные права у водителя
        DriversLicense driversLicense = driver.getDriversLicense();
        String driversLicenseType = driversLicense.getLicenseType().toString();

        if (!this.checkPermissions(vehicleType, driversLicenseType)) {
            throw new InvalidLicenseCategoryException();
        }

        if (!this.checkLicenseDate(driversLicense.getDateOfExpiry())) {
            throw new LicenseExpiredException();
        }

        vehicle.setDriverId(driverId);
        this.vehicleService.saveVehicle(vehicle);
    }

    /**
     * Метод удаления связи авто с водителем
     * @param driverId Id водителя
     * @param vehicleId Id авто
     */
    @Override
    public void detachVehicle(Long driverId, Long vehicleId) {
        Driver driver = repository.findById(driverId)
                .orElseThrow(() -> new DriverNotFoundException(driverId));

        // получаем тип авто
        Vehicle vehicle = vehicleService.findVehicleById(vehicleId);
        vehicle.setDriverId(null);
        this.vehicleService.saveVehicle(vehicle);
    }

    /**
     * Проверяем что права не просрочены
     * @param date Дата действия до у прав
     * @return bool Просрочены или нет
     */
    private Boolean checkLicenseDate(Date date) {
        return date.after(new Date());
    }

    /**
     * Проверяем может ли водитель водить автомобиль указанной категории
     * @param vehicleType Тип авто
     * @param driversLicenseType Категория прав
     * @return bool
     */
    private Boolean checkPermissions(String vehicleType, String driversLicenseType) {
        HashMap<String, HashMap<String, Boolean>> permissions = new HashMap<>();
        // Для того чтобы избавиться от кучи If else
        permissions.put("B", new HashMap<>());
        permissions.put("C", new HashMap<>());
        permissions.put("D", new HashMap<>());
        // категория B дает разрешение на закреп только обычных авто
        permissions.get("B").put("PASSENGER", true);
        // категория C дает разрешение на закреп обычных авто и грузовиков
        permissions.get("C").put("PASSENGER", true);
        permissions.get("C").put("CARGO", true);
        // категория D дает разрешение на закреп обычных авто и автобусов
        permissions.get("D").put("PASSENGER", true);
        permissions.get("D").put("BUS", true);

        return permissions.get(driversLicenseType).containsKey(vehicleType);
    }
}
