package com.example.test.service;

import com.example.test.exceptions.VehicleNotFoundException;
import com.example.test.model.Vehicle;

import java.util.List;

/**
 * Сервис с бизнес логикой по работе с таблицей автомобилей
 */
public interface VehicleService {
    public List<Vehicle> findAll();

    public Vehicle findVehicleById(Long id) throws VehicleNotFoundException;

    public Vehicle saveVehicle(Vehicle vehicle);

    public void deleteVehicleById(Long id) throws VehicleNotFoundException;
}
