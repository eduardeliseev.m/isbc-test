package com.example.test.service;

import com.example.test.exceptions.DriversLicenseNotFoundException;
import com.example.test.model.DriversLicense;

import java.util.List;

/**
 * Сервис с бизнес логикой по работе с правами водителя
 */
public interface DriversLicenseService {
    public List<DriversLicense> findAll();

    public DriversLicense findDriversLicenseById(Long id) throws DriversLicenseNotFoundException;

    public DriversLicense saveDriversLicense(DriversLicense driver);

    public void deleteDriversLicenseById(Long id) throws DriversLicenseNotFoundException;
}
