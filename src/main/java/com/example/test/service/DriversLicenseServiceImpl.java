package com.example.test.service;

import com.example.test.exceptions.DriversLicenseNotFoundException;
import com.example.test.model.DriversLicense;
import com.example.test.repository.DriversLicenseCrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DriversLicenseServiceImpl implements DriversLicenseService {
    private final DriversLicenseCrudRepository repository;

    public DriversLicenseServiceImpl(DriversLicenseCrudRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<DriversLicense> findAll() {
        return repository.findAll();
    }

    @Override
    public DriversLicense findDriversLicenseById(Long id) throws DriversLicenseNotFoundException {
        return repository.findById(id)
                .orElseThrow(() -> new DriversLicenseNotFoundException(id));
    }

    @Override
    public DriversLicense saveDriversLicense(DriversLicense driver) {
        return repository.save(driver);
    }

    @Override
    public void deleteDriversLicenseById(Long id) throws DriversLicenseNotFoundException {
        repository.delete(this.findDriversLicenseById(id));
    }

            // Если данные по правам не переданы, то ничего не меняем
        // Иначе
        // Ищем права с переданными данными
        // Если не находим, то значит нам пришли новые права
        // Создаем новые права
        // Все старые аннулируем

}
