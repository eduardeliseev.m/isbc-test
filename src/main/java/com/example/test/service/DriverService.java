package com.example.test.service;

import com.example.test.exceptions.DriverNotFoundException;
import com.example.test.model.Driver;

import java.util.List;

/**
 * Сервис с бизнес логикой по работе с водителем
 */
public interface DriverService {
    List<Driver> findAll();

    Driver findDriverById(Long id) throws DriverNotFoundException;

    Driver saveDriver(Driver driver);

    void deleteDriverById(Long id) throws DriverNotFoundException;

    void attachVehicle(Long driverId, Long vehicleId);

    void detachVehicle(Long driverId, Long vehicleId);
}
