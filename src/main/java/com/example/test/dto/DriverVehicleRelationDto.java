package com.example.test.dto;

/**
 * DTO Связывающий водителя и авто
 */
public class DriverVehicleRelationDto {
    private Long driverId;
    private Long vehicleId;

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }
}
