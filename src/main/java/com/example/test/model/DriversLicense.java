package com.example.test.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "drivers_licenses")
public class DriversLicense {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="drivers_license_id")
    private Long driversLicenseId;

    @OneToOne(mappedBy="driversLicense")
    private Driver driver;

    @Column(name="date_of_issue", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfIssue;

    @Column(name="date_of_expiry", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfExpiry;

    @Enumerated(EnumType.STRING)
    @Column(name="license_type")
    private LicenseType licenseType;

    public Long getDriversLicenseId() {
        return driversLicenseId;
    }

    public void setDriversLicenseId(Long driversLicenseId) {
        this.driversLicenseId = driversLicenseId;
    }

    public Date getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(Date dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public Date getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(Date dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }

    public LicenseType getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(LicenseType licenseType) {
        this.licenseType = licenseType;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public enum LicenseType {
        B,
        C,
        D,
    }

    public DriversLicense() {

    }

    public DriversLicense(Date dateOfIssue, Date dateOfExpiry) {
        this.setDateOfIssue(dateOfIssue);
        this.setDateOfExpiry(dateOfExpiry);
    }
}
