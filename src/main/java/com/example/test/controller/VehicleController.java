package com.example.test.controller;

import com.example.test.dto.VehicleRequestDto;
import com.example.test.dto.VehicleResponseDto;
import com.example.test.mapper.VehicleMapper;
import com.example.test.model.Vehicle;
import com.example.test.service.VehicleServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * MVC Контроллер для обработки запросов на крад автомобилей
 * @version 1.0
 */
@RestController
public class VehicleController {

    private final VehicleServiceImpl vehicleService;

    private final VehicleMapper vehicleMapper;

    public VehicleController(VehicleServiceImpl vehicleService, VehicleMapper vehicleMapper) {
        this.vehicleService = vehicleService;
        this.vehicleMapper = vehicleMapper;
    }

    @Operation(summary = "Метод вывода всех водителей")
    @GetMapping("/vehicles")
    List<VehicleResponseDto> all() {
        List<Vehicle> vehicles = this.vehicleService.findAll();
        return vehicles.stream()
          .map(this.vehicleMapper::convertToDto)
          .collect(Collectors.toList());
    }

    @Operation(summary = "Метод создания авто")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Создано",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = VehicleRequestDto.class)) }),
        @ApiResponse(responseCode = "400", description = "Передан невалидный DTO",
            content = @Content)})
    @PostMapping("/vehicles")
    VehicleResponseDto createVehicle(@RequestBody VehicleRequestDto newVehicleDto) {
        Vehicle vehicle;
        vehicle = this.vehicleMapper.convertToEntity(newVehicleDto);
        vehicle = this.vehicleService.saveVehicle(vehicle);
        return this.vehicleMapper.convertToDto(vehicle);
    }

    @Operation(summary = "Метод поиска авто")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Успешно",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = VehicleResponseDto.class)) }),
        @ApiResponse(responseCode = "400", description = "Передан невалидный ID",
            content = @Content),
        @ApiResponse(responseCode = "404", description = "Авто не найден",
            content = @Content) })
    @GetMapping("/vehicles/{id}")
    VehicleResponseDto one(@PathVariable Long id) {
        Vehicle vehicle = this.vehicleService.findVehicleById(id);
        return this.vehicleMapper.convertToDto(vehicle);
    }

    @Operation(summary = "Метод обновления авто")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Обновлено",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = VehicleRequestDto.class)) }),
        @ApiResponse(responseCode = "400", description = "Передан невалидный DTO",
            content = @Content),
        @ApiResponse(responseCode = "404", description = "Авто не найдено",
            content = @Content) })
    @PutMapping("/vehicles/{id}")
    void updateVehicle(@RequestBody VehicleRequestDto vehicleDto, @PathVariable Long id) {
        Vehicle vehicle = this.vehicleService.findVehicleById(id);

        vehicle = this.vehicleMapper.convertToEntity(vehicleDto, vehicle);
        this.vehicleService.saveVehicle(vehicle);
    }

    @Operation(summary = "Метод удаления авто")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Удалено",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = Long.class)) }),
        @ApiResponse(responseCode = "404", description = "Авто не найден",
            content = @Content) })
    @DeleteMapping("/vehicles/{id}")
    void deleteVehicle(@PathVariable Long id) {
        this.vehicleService.deleteVehicleById(id);
    }

}
