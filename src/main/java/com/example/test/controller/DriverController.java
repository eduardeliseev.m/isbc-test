package com.example.test.controller;

import com.example.test.dto.DriverRequestDto;
import com.example.test.dto.DriverResponseDto;
import com.example.test.dto.DriverVehicleRelationDto;
import com.example.test.dto.DriversLicenseRequestDto;
import com.example.test.mapper.DriverMapper;
import com.example.test.mapper.DriversLicenseMapper;
import com.example.test.model.Driver;
import com.example.test.model.DriversLicense;
import com.example.test.service.DriverServiceImpl;
import com.example.test.service.DriversLicenseServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * MVC Контроллер для обработки запросов на крад водителей
 * @version 1.0
 */
@RestController
public class DriverController {
    private static final Logger logger = LogManager.getLogger(DriverController.class);

    private final DriverServiceImpl driverService;

    private final DriversLicenseServiceImpl driversLicenseService;

    private final DriverMapper driverMapper;

    private final DriversLicenseMapper driversLicenseMapper;

    public DriverController(DriverServiceImpl driverService,
                            DriversLicenseServiceImpl driversLicenseService,
                            DriverMapper driverMapper,
                            DriversLicenseMapper driversLicenseMapper) {
        this.driverService = driverService;
        this.driversLicenseService = driversLicenseService;
        this.driverMapper = driverMapper;
        this.driversLicenseMapper = driversLicenseMapper;
    }


    @Operation(summary = "Метод вывода всех водителей")
    @GetMapping("/drivers")
    List<DriverResponseDto> all() {
        List<Driver> drivers = this.driverService.findAll();
        return drivers.stream()
          .map(this.driverMapper::convertToDto)
          .collect(Collectors.toList());
    }

    @Operation(summary = "Метод создания водителя")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Успешно",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = DriverRequestDto.class)) }),
        @ApiResponse(responseCode = "400", description = "Передан невалидный DTO",
            content = @Content)})
    @PostMapping("/drivers")
    DriverResponseDto createDriver(@RequestBody DriverRequestDto newDriverDto) {
        logger.info("Received driver creation Request: name: " + newDriverDto.toString());
        Driver driver;

        DriversLicenseRequestDto driverLicenseDto = newDriverDto.getDriversLicense();
        driver = this.driverMapper.convertToEntity(newDriverDto);
        // на случай если у водителя нет прав
        if (driverLicenseDto != null) {
            DriversLicense driverLicense = this.driversLicenseMapper.convertToEntity(driverLicenseDto);
            driverLicense = this.driversLicenseService.saveDriversLicense(driverLicense);
            driver.setDriversLicense(driverLicense);
        }

        driver = this.driverService.saveDriver(driver);
        return this.driverMapper.convertToDto(driver);
    }

    @Operation(summary = "Метод поиска водителя")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Успешно",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = DriverResponseDto.class)) }),
        @ApiResponse(responseCode = "400", description = "Передан невалидный ID",
            content = @Content),
        @ApiResponse(responseCode = "404", description = "Водитель не найден",
            content = @Content) })
    @GetMapping("/drivers/{id}")
    DriverResponseDto one(@PathVariable Long id) {
        Driver driver = this.driverService.findDriverById(id);
        return this.driverMapper.convertToDto(driver);
    }

    @Operation(summary = "Метод обновления водителя")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Обновлено",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = DriverRequestDto.class)) }),
        @ApiResponse(responseCode = "400", description = "Передан невалидный DTO",
            content = @Content),
        @ApiResponse(responseCode = "404", description = "Водитель не найден",
            content = @Content) })
    @PutMapping("/drivers/{id}")
    void updateDriver(@RequestBody DriverRequestDto driverDto, @PathVariable Long id) {
        // находим сущность
        Driver driver = this.driverService.findDriverById(id);
        // вставляем новые значения
        driver = this.driverMapper.convertToEntity(driverDto, driver);
        DriversLicenseRequestDto driversLicenseDto = driverDto.getDriversLicense();

        if (driversLicenseDto != null) {
            // находим данные по правам, параллельно их обновляя
            DriversLicense driverLicense = this.driversLicenseMapper.convertToEntity(driversLicenseDto,
                    driver.getDriversLicense());
             // сохраняем права
            driverLicense = this.driversLicenseService.saveDriversLicense(driverLicense);
            driver.setDriversLicense(driverLicense);
        }

        this.driverService.saveDriver(driver);
    }

    @Operation(summary = "Метод удаления водителя")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Удалено",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = Long.class)) }),
        @ApiResponse(responseCode = "404", description = "Водитель не найден",
            content = @Content) })
    @DeleteMapping("/drivers/{id}")
    void deleteDriver(@PathVariable Long id) {
        this.driverService.deleteDriverById(id);
    }

    /**
     * Метод привязки авто к водителю
     * @param relationDto сущность в виде dto, где
     *  driverId - Идентификатор водителя
     *  vehicleId - Идентификатор авто
     */
    @Operation(summary = "Метод привязки авто к водителю")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Связано",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = DriverVehicleRelationDto.class)) }),
        @ApiResponse(responseCode = "400", description = "Ошибка создания связи",
            content = @Content),
        @ApiResponse(responseCode = "404", description = "Водитель или Авто не найдены",
            content = @Content) })
    @PostMapping("/attach-vehicle/")
    void attachVehicle(@RequestBody DriverVehicleRelationDto relationDto) {
        this.driverService.attachVehicle(relationDto.getDriverId(), relationDto.getVehicleId());
    }

    /**
     * Метод удаления связи авти и водителю
     * @param relationDto сущность в виде dto, где
     *  driverId - Идентификатор водителя
     *  vehicleId - Идентификатор авто
     */
    @Operation(summary = "Метод удаления связи авти и водителю")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Связь удалена",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = DriverVehicleRelationDto.class)) }),
        @ApiResponse(responseCode = "404", description = "Водитель или Авто не найдены",
            content = @Content) })
    @PostMapping("/detach-vehicle/")
    void detachVehicle(@RequestBody DriverVehicleRelationDto relationDto) {
        this.driverService.detachVehicle(relationDto.getDriverId(), relationDto.getVehicleId());
    }
}
