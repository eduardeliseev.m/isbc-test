-- Создание таблицы для хранения записей о водителях
CREATE TABLE drivers (
    driver_id serial PRIMARY KEY,
    first_name varchar(20) not null,
    middle_name varchar(20),
    last_name varchar(20) not null,
    email varchar(50),
    phone varchar(50) not null,
    date_of_birth timestamp not null
);
