-- Создание таблицы для хранения записей о водительских лицензиях
CREATE TYPE drivers_license_types AS ENUM ('B', 'C', 'D');
CREATE TABLE IF NOT EXISTS drivers_licenses (
    drivers_license_id serial PRIMARY KEY,
    license_type drivers_licence_types,
    date_of_issue timestamp not null,
    date_of_expiry timestamp not null
);
