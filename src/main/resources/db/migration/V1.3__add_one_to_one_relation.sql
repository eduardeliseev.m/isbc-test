ALTER TABLE drivers ADD COLUMN drivers_license_id INTEGER;
ALTER TABLE drivers
   ADD CONSTRAINT fk_driverLicense
   FOREIGN KEY (drivers_license_id)
   REFERENCES drivers_licenses(drivers_license_id);
