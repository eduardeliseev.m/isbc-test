-- Создание таблицы для хранения записей об авто
CREATE TYPE vehicles_types AS ENUM('passenger', 'cargo', 'bus');
CREATE TABLE IF NOT EXISTS vehicles (
    vehicle_id serial PRIMARY KEY,
    driver_id int,
    vehicle_type vehicles_types,
    date_of_creation timestamp not null,
    FOREIGN KEY (driver_id)
      REFERENCES drivers (driver_id)
);
