FROM amazoncorretto:11-alpine-jdk
MAINTAINER epimetheus84
COPY target/test-0.0.1-SNAPSHOT.jar app-1.0.0.jar
ENTRYPOINT ["java","-jar","/app-1.0.0.jar"]