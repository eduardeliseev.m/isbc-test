CREATE USER isbc WITH encrypted password 'admin';
CREATE DATABASE isbc;
GRANT ALL PRIVILEGES ON DATABASE isbc TO isbc;